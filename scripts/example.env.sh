# Craft Scripts Environment
# @author    nystudio107
# @copyright Copyright (c) 2016 nystudio107
# @link      https://nystudio107.com/
# @package   craft-scripts-environment
# @since     1.0.1
# @license   MIT
#
# This file should be renamed to '.env.sh' and it should reside in the
# `scripts` directory.  Add '.env.sh' to your .gitignore.

# Local environmental config for nystudio107 Craft scripts

# -- LOCAL settings --

# Local path constants; paths should always have a trailing /
LOCAL_ROOT_PATH="/Applications/MAMP/htdocs/REPLACE_ME"
LOCAL_ASSETS_PATH=$LOCAL_ROOT_PATH"public/media"

# Local user & group that should own the Craft CMS install
LOCAL_CHOWN_USER="admin"
LOCAL_CHOWN_GROUP="apache"

# Local directories relative to LOCAL_ROOT_PATH that should be writeable by the $CHOWN_GROUP
LOCAL_WRITEABLE_DIRS=(
                "craft/storage"
                "public/assets"
                )

# Local asset directories relative to LOCAL_ASSETS_PATH that should be synched with remote assets
LOCAL_ASSETS_DIRS=(
                ""
                )

# Local database constants
LOCAL_DB_NAME="craft"
LOCAL_DB_PASSWORD="root"
LOCAL_DB_USER="root"

# These are here for MAMP, which requires a full path to the `mysql` executable inside the application package
LOCAL_MYSQL_CMD="/Applications/MAMP/Library/bin/mysql"
LOCAL_MYSQLDUMP_CMD="/Applications/MAMP/Library/bin/mysqldump"

# -- REMOTE settings --

# Remote ssh credentials, user@domain.com and Remote SSH Port
REMOTE_SSH_LOGIN="root@163.209.94.252"
REMOTE_SSH_PORT="22"

# Remote path constants; paths should always have a trailing /
REMOTE_ROOT_PATH="/var/www/vhosts/"
REMOTE_ASSETS_PATH=$REMOTE_ROOT_PATH"public/media"

# Remote database constants
REMOTE_DB_NAME="REPLACE_ME"
REMOTE_DB_PASSWORD="REPLACE_ME"
REMOTE_DB_USER="REPLACE_ME"
REMOTE_DB_HOST="127.0.0.1"
REMOTE_DB_PORT="3306"
