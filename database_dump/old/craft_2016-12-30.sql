# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: craft
# Generation Time: 2016-12-30 13:32:42 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table craft_assetfiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetfiles`;

CREATE TABLE `craft_assetfiles` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetfiles_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `craft_assetfiles_sourceId_fk` (`sourceId`),
  KEY `craft_assetfiles_folderId_fk` (`folderId`),
  CONSTRAINT `craft_assetfiles_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `craft_assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assetfiles_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assetfiles_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assetfiles` WRITE;
/*!40000 ALTER TABLE `craft_assetfiles` DISABLE KEYS */;

INSERT INTO `craft_assetfiles` (`id`, `sourceId`, `folderId`, `filename`, `kind`, `width`, `height`, `size`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(11,1,1,'Screen-Shot-2016-12-20-at-12.31.37-PM.png','image',374,444,24283,'2016-12-20 22:21:43','2016-12-20 22:21:43','2016-12-20 22:21:43','ec7c9570-ddc2-4a1e-b35a-1903782b2053'),
	(12,1,1,'Screen-Shot-2016-12-20-at-9.42.04-AM.png','image',797,551,131099,'2016-12-20 22:21:44','2016-12-20 22:21:44','2016-12-20 22:21:44','67c26b69-3394-49c2-b16d-eaf6cd30dbc2'),
	(13,1,1,'Screen-Shot-2016-12-19-at-4.40.10-PM.png','image',633,295,50996,'2016-12-20 22:21:44','2016-12-20 22:21:44','2016-12-20 22:21:44','ddcb2467-0102-42d0-ade4-3d660f9e1dbd');

/*!40000 ALTER TABLE `craft_assetfiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assetfolders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetfolders`;

CREATE TABLE `craft_assetfolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetfolders_name_parentId_sourceId_unq_idx` (`name`,`parentId`,`sourceId`),
  KEY `craft_assetfolders_parentId_fk` (`parentId`),
  KEY `craft_assetfolders_sourceId_fk` (`sourceId`),
  CONSTRAINT `craft_assetfolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `craft_assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assetfolders_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assetfolders` WRITE;
/*!40000 ALTER TABLE `craft_assetfolders` DISABLE KEYS */;

INSERT INTO `craft_assetfolders` (`id`, `parentId`, `sourceId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,1,'Media','','2016-12-20 22:19:47','2016-12-20 22:19:47','cbdce5f8-b1c2-42e7-aa3f-6ed4267cbfa7'),
	(2,NULL,NULL,'Temporary source',NULL,'2016-12-20 22:20:37','2016-12-20 22:20:37','9e8765a5-9bf4-4e98-ab16-6d9a9505c3a7'),
	(3,2,NULL,'user_1',NULL,'2016-12-20 22:20:37','2016-12-20 22:20:37','fde947d4-416e-4cb7-87de-3e3eed61ded1'),
	(4,3,NULL,'field_28','field_28/','2016-12-20 22:20:37','2016-12-20 22:20:37','7392485a-a4e9-4bb8-90eb-8cb70643833e');

/*!40000 ALTER TABLE `craft_assetfolders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assetindexdata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetindexdata`;

CREATE TABLE `craft_assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sourceId` int(10) NOT NULL,
  `offset` int(10) NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recordId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetindexdata_sessionId_sourceId_offset_unq_idx` (`sessionId`,`sourceId`,`offset`),
  KEY `craft_assetindexdata_sourceId_fk` (`sourceId`),
  CONSTRAINT `craft_assetindexdata_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_assetsources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetsources`;

CREATE TABLE `craft_assetsources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetsources_name_unq_idx` (`name`),
  UNIQUE KEY `craft_assetsources_handle_unq_idx` (`handle`),
  KEY `craft_assetsources_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_assetsources_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assetsources` WRITE;
/*!40000 ALTER TABLE `craft_assetsources` DISABLE KEYS */;

INSERT INTO `craft_assetsources` (`id`, `name`, `handle`, `type`, `settings`, `sortOrder`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Media','media','Local','{\"path\":\"{basePath}resources\\/media\\/\",\"publicURLs\":\"1\",\"url\":\"\\/resources\\/media\\/\"}',1,54,'2016-12-20 22:19:47','2016-12-20 23:18:30','414779dc-2bf4-48e5-b1b9-b59f8849e6a6');

/*!40000 ALTER TABLE `craft_assetsources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assettransformindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assettransformindex`;

CREATE TABLE `craft_assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT NULL,
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_assettransformindex_sourceId_fileId_location_idx` (`sourceId`,`fileId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assettransformindex` WRITE;
/*!40000 ALTER TABLE `craft_assettransformindex` DISABLE KEYS */;

INSERT INTO `craft_assettransformindex` (`id`, `fileId`, `filename`, `format`, `location`, `sourceId`, `fileExists`, `inProgress`, `dateIndexed`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,13,'Screen-Shot-2016-12-19-at-4.40.10-PM.png',NULL,'_thumbnail',1,1,0,'2016-12-20 23:18:33','2016-12-20 23:18:33','2016-12-20 23:18:33','219e5a71-f496-474f-a496-9a4dcd22894c'),
	(2,11,'Screen-Shot-2016-12-20-at-12.31.37-PM.png',NULL,'_thumbnail',1,1,0,'2016-12-20 23:18:33','2016-12-20 23:18:33','2016-12-20 23:18:33','cd75a4c9-1c2a-42a0-b3ca-f0e1f07536c6'),
	(3,12,'Screen-Shot-2016-12-20-at-9.42.04-AM.png',NULL,'_thumbnail',1,1,0,'2016-12-20 23:18:33','2016-12-20 23:18:33','2016-12-20 23:18:33','24834187-f62e-4b7c-968a-477985d5466d');

/*!40000 ALTER TABLE `craft_assettransformindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assettransforms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assettransforms`;

CREATE TABLE `craft_assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `height` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int(10) DEFAULT NULL,
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `craft_assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assettransforms` WRITE;
/*!40000 ALTER TABLE `craft_assettransforms` DISABLE KEYS */;

INSERT INTO `craft_assettransforms` (`id`, `name`, `handle`, `mode`, `position`, `height`, `width`, `format`, `quality`, `dimensionChangeTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Thumbnail','thumbnail','crop','center-center',300,300,NULL,NULL,'2016-12-20 22:25:21','2016-12-20 22:25:21','2016-12-20 22:25:21','bfe27551-1d3d-43d6-8d57-a89e6b63520e');

/*!40000 ALTER TABLE `craft_assettransforms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categories`;

CREATE TABLE `craft_categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_categories_groupId_fk` (`groupId`),
  CONSTRAINT `craft_categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_categories_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_categorygroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categorygroups`;

CREATE TABLE `craft_categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_categorygroups_handle_unq_idx` (`handle`),
  KEY `craft_categorygroups_structureId_fk` (`structureId`),
  KEY `craft_categorygroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_categorygroups_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categorygroups_i18n`;

CREATE TABLE `craft_categorygroups_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_categorygroups_i18n_groupId_locale_unq_idx` (`groupId`,`locale`),
  KEY `craft_categorygroups_i18n_locale_fk` (`locale`),
  CONSTRAINT `craft_categorygroups_i18n_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_categorygroups_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_content`;

CREATE TABLE `craft_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_body` text COLLATE utf8_unicode_ci,
  `field_flickr` text COLLATE utf8_unicode_ci,
  `field_facebook` text COLLATE utf8_unicode_ci,
  `field_twitter` text COLLATE utf8_unicode_ci,
  `field_google` text COLLATE utf8_unicode_ci,
  `field_pinterest` text COLLATE utf8_unicode_ci,
  `field_linkedin` text COLLATE utf8_unicode_ci,
  `field_primaryTelephone` text COLLATE utf8_unicode_ci,
  `field_gaCode` text COLLATE utf8_unicode_ci,
  `field_primaryEmail` text COLLATE utf8_unicode_ci,
  `field_defaultSeo` text COLLATE utf8_unicode_ci,
  `field_instagram` text COLLATE utf8_unicode_ci,
  `field_pageDescription` text COLLATE utf8_unicode_ci,
  `field_street` text COLLATE utf8_unicode_ci,
  `field_city` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_content_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `craft_content_title_idx` (`title`),
  KEY `craft_content_locale_fk` (`locale`),
  CONSTRAINT `craft_content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_content_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_content` WRITE;
/*!40000 ALTER TABLE `craft_content` DISABLE KEYS */;

INSERT INTO `craft_content` (`id`, `elementId`, `locale`, `title`, `field_body`, `field_flickr`, `field_facebook`, `field_twitter`, `field_google`, `field_pinterest`, `field_linkedin`, `field_primaryTelephone`, `field_gaCode`, `field_primaryEmail`, `field_defaultSeo`, `field_instagram`, `field_pageDescription`, `field_street`, `field_city`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 19:47:26','2016-12-20 19:47:26','dd9defae-6b20-49be-8ad7-0fd37929d8c8'),
	(2,2,'en_us','Welcome to Local.craft.dev!','<p>It’s true, this site doesn’t have a whole lot of content yet, but don’t worry. Our web developers have just installed the CMS, and they’re setting things up for the content editors this very moment. Soon Local.craft.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.</p>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'2016-12-20 19:47:28','2016-12-20 22:21:48','059eb738-9627-40cd-9301-e5a01c3040d7'),
	(3,3,'en_us','We just installed Craft!','<p>Craft is the CMS that’s powering Local.craft.dev. It’s beautiful, powerful, flexible, and easy-to-use, and it’s made by Pixel &amp; Tonic. We can’t wait to dive in and see what it’s capable of!</p><!--pagebreak--><p>This is even more captivating content, which you couldn’t see on the News index page because it was entered after a Page Break, and the News index template only likes to show the content on the first page.</p><p>Craft: a nice alternative to Word, if you’re making a website.</p>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 19:47:28','2016-12-20 19:47:28','403b195d-fd20-4d83-b10c-df6e58e1fa29'),
	(4,4,'en_us',NULL,NULL,'','test','','','','',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'2016-12-20 19:48:11','2016-12-20 19:57:41','2151d3c7-7491-441e-ba44-c2b8c7df3128'),
	(5,5,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','test','','',NULL,NULL,NULL,NULL,'2016-12-20 19:53:48','2016-12-20 19:55:51','6d8dff9f-9b99-4150-af1f-f3a4a1434397'),
	(6,11,'en_us','Screen Shot 2016 12 20 At 12 31 37 Pm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 22:21:43','2016-12-20 22:21:43','e7dc57bb-c680-4cd8-93fd-48e0a6262304'),
	(7,12,'en_us','Screen Shot 2016 12 20 At 9 42 04 Am',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 22:21:44','2016-12-20 22:21:44','f5dc9fbf-2a6b-4f1a-bdbd-e3f44d635edc'),
	(8,13,'en_us','Screen Shot 2016 12 19 At 4 40 10 Pm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 22:21:44','2016-12-20 22:21:44','ce6a19a0-72fe-4c3c-a9fe-d7442e4dba84');

/*!40000 ALTER TABLE `craft_content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_deprecationerrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_deprecationerrors`;

CREATE TABLE `craft_deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint(6) unsigned NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `templateLine` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_elementindexsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elementindexsettings`;

CREATE TABLE `craft_elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elements`;

CREATE TABLE `craft_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_elements_type_idx` (`type`),
  KEY `craft_elements_enabled_idx` (`enabled`),
  KEY `craft_elements_archived_dateCreated_idx` (`archived`,`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_elements` WRITE;
/*!40000 ALTER TABLE `craft_elements` DISABLE KEYS */;

INSERT INTO `craft_elements` (`id`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'User',1,0,'2016-12-20 19:47:26','2016-12-20 19:47:26','b2a72b10-5615-4fcf-b634-82e0872980ae'),
	(2,'Entry',1,0,'2016-12-20 19:47:28','2016-12-20 22:21:48','2db1a6c1-cbd9-4b73-a1af-b040d83519b4'),
	(3,'Entry',1,0,'2016-12-20 19:47:28','2016-12-20 19:47:28','a30b975f-a5ba-4554-a742-37f04b6ac8b5'),
	(4,'GlobalSet',1,0,'2016-12-20 19:48:11','2016-12-20 19:57:41','853273e6-f1d8-4d52-9f56-40c9b7482f1d'),
	(5,'GlobalSet',1,0,'2016-12-20 19:53:48','2016-12-20 19:55:51','25b4e62e-9903-481c-89b0-e4117f13f434'),
	(6,'MatrixBlock',1,0,'2016-12-20 21:17:19','2016-12-20 22:21:48','2c68403d-0dda-4ccb-b098-e0e787499009'),
	(7,'MatrixBlock',1,0,'2016-12-20 21:17:19','2016-12-20 22:21:48','504593cc-9028-4f20-af26-d91a58afded4'),
	(8,'MatrixBlock',1,0,'2016-12-20 21:52:55','2016-12-20 22:21:48','58313d54-1677-41ea-883f-b9dca36290cd'),
	(9,'MatrixBlock',1,0,'2016-12-20 21:53:39','2016-12-20 22:21:48','7318b27e-a3f4-4320-8194-113c855d5209'),
	(10,'MatrixBlock',1,0,'2016-12-20 22:18:51','2016-12-20 22:21:48','17c76b96-edb5-4409-8eac-5c0728e683c3'),
	(11,'Asset',1,0,'2016-12-20 22:21:43','2016-12-20 22:21:43','cf7ce9f4-170c-4e0d-8c91-5c5602244ae7'),
	(12,'Asset',1,0,'2016-12-20 22:21:44','2016-12-20 22:21:44','9c4f9fe2-27b0-472a-8bec-ef1193d9d585'),
	(13,'Asset',1,0,'2016-12-20 22:21:44','2016-12-20 22:21:44','68a06648-fb3e-4564-812e-7f064fc0d5b8');

/*!40000 ALTER TABLE `craft_elements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_elements_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elements_i18n`;

CREATE TABLE `craft_elements_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_elements_i18n_elementId_locale_unq_idx` (`elementId`,`locale`),
  UNIQUE KEY `craft_elements_i18n_uri_locale_unq_idx` (`uri`,`locale`),
  KEY `craft_elements_i18n_slug_locale_idx` (`slug`,`locale`),
  KEY `craft_elements_i18n_enabled_idx` (`enabled`),
  KEY `craft_elements_i18n_locale_fk` (`locale`),
  CONSTRAINT `craft_elements_i18n_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_elements_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_elements_i18n` WRITE;
/*!40000 ALTER TABLE `craft_elements_i18n` DISABLE KEYS */;

INSERT INTO `craft_elements_i18n` (`id`, `elementId`, `locale`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us','',NULL,1,'2016-12-20 19:47:26','2016-12-20 19:47:26','90c86e13-2ec5-451c-8e4c-13253d5a7c31'),
	(2,2,'en_us','homepage','__home__',1,'2016-12-20 19:47:28','2016-12-20 22:21:48','60896d95-d056-43bd-acbc-27a6298e5ab4'),
	(3,3,'en_us','we-just-installed-craft','news/2016/we-just-installed-craft',1,'2016-12-20 19:47:29','2016-12-20 19:47:29','69db4aed-bf71-4240-a12b-fd18f70a576f'),
	(4,4,'en_us','',NULL,1,'2016-12-20 19:48:11','2016-12-20 19:57:41','948dbf65-3342-45fc-9fe9-e41012489e30'),
	(5,5,'en_us','',NULL,1,'2016-12-20 19:53:48','2016-12-20 19:55:51','e0675fea-13b4-4944-95e7-54efa7f042f8'),
	(6,6,'en_us','',NULL,1,'2016-12-20 21:17:19','2016-12-20 22:21:48','a0901faf-450b-4f45-8e06-2f155f879f45'),
	(7,7,'en_us','',NULL,1,'2016-12-20 21:17:19','2016-12-20 22:21:48','b836e61f-f9a9-4911-b60d-e68f04f61cd6'),
	(8,8,'en_us','',NULL,1,'2016-12-20 21:52:55','2016-12-20 22:21:48','cf1ce6ef-19ef-45d8-a81e-8dd00ed6b000'),
	(9,9,'en_us','',NULL,1,'2016-12-20 21:53:39','2016-12-20 22:21:48','d7fb727e-2628-4d07-aaf7-7e62718c27d7'),
	(10,10,'en_us','',NULL,1,'2016-12-20 22:18:51','2016-12-20 22:21:48','de17cdb3-d26a-4533-81c4-612f84c0fc23'),
	(11,11,'en_us','screen-shot-2016-12-20-at-12-31-37-pm',NULL,1,'2016-12-20 22:21:43','2016-12-20 22:21:43','536768cd-4f3f-4bc4-a0bb-ab3a04ef669f'),
	(12,12,'en_us','screen-shot-2016-12-20-at-9-42-04-am',NULL,1,'2016-12-20 22:21:44','2016-12-20 22:21:44','c7c0a69c-f3b6-4b4f-8eab-29f8b42156b6'),
	(13,13,'en_us','screen-shot-2016-12-19-at-4-40-10-pm',NULL,1,'2016-12-20 22:21:44','2016-12-20 22:21:44','b74a0933-195d-4df9-9041-c260dc878906');

/*!40000 ALTER TABLE `craft_elements_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_emailmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_emailmessages`;

CREATE TABLE `craft_emailmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_emailmessages_key_locale_unq_idx` (`key`,`locale`),
  KEY `craft_emailmessages_locale_fk` (`locale`),
  CONSTRAINT `craft_emailmessages_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entries`;

CREATE TABLE `craft_entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entries_sectionId_idx` (`sectionId`),
  KEY `craft_entries_typeId_idx` (`typeId`),
  KEY `craft_entries_postDate_idx` (`postDate`),
  KEY `craft_entries_expiryDate_idx` (`expiryDate`),
  KEY `craft_entries_authorId_fk` (`authorId`),
  CONSTRAINT `craft_entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entries` WRITE;
/*!40000 ALTER TABLE `craft_entries` DISABLE KEYS */;

INSERT INTO `craft_entries` (`id`, `sectionId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,1,NULL,NULL,'2016-12-20 19:47:28',NULL,'2016-12-20 19:47:28','2016-12-20 22:21:48','c7c7113d-4e69-49d6-976e-9f8c383198c9'),
	(3,2,2,1,'2016-12-20 19:47:28',NULL,'2016-12-20 19:47:29','2016-12-20 19:47:29','de66bab2-77d9-4d13-93d9-19636159b713');

/*!40000 ALTER TABLE `craft_entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_entrydrafts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entrydrafts`;

CREATE TABLE `craft_entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entrydrafts_entryId_locale_idx` (`entryId`,`locale`),
  KEY `craft_entrydrafts_sectionId_fk` (`sectionId`),
  KEY `craft_entrydrafts_creatorId_fk` (`creatorId`),
  KEY `craft_entrydrafts_locale_fk` (`locale`),
  CONSTRAINT `craft_entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entrydrafts_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_entrytypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entrytypes`;

CREATE TABLE `craft_entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Title',
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `craft_entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `craft_entrytypes_sectionId_fk` (`sectionId`),
  KEY `craft_entrytypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entrytypes` WRITE;
/*!40000 ALTER TABLE `craft_entrytypes` DISABLE KEYS */;

INSERT INTO `craft_entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,14,'Homepage','homepage',1,'Title',NULL,1,'2016-12-20 19:47:28','2016-12-20 21:10:17','e9043742-add2-489e-bbcc-22c11fa52d09'),
	(2,2,5,'News','news',1,'Title',NULL,1,'2016-12-20 19:47:28','2016-12-20 19:47:28','37ded7fa-6786-4a87-980e-09b27ad498e8');

/*!40000 ALTER TABLE `craft_entrytypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_entryversions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entryversions`;

CREATE TABLE `craft_entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entryversions_entryId_locale_idx` (`entryId`,`locale`),
  KEY `craft_entryversions_sectionId_fk` (`sectionId`),
  KEY `craft_entryversions_creatorId_fk` (`creatorId`),
  KEY `craft_entryversions_locale_fk` (`locale`),
  CONSTRAINT `craft_entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entryversions_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entryversions` WRITE;
/*!40000 ALTER TABLE `craft_entryversions` DISABLE KEYS */;

INSERT INTO `craft_entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `locale`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,2,1,1,'en_us',1,NULL,'{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2016-12-20 19:47:28','2016-12-20 19:47:28','974f0055-acdb-42fe-97c6-270427f92c76'),
	(2,2,1,1,'en_us',2,NULL,'{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":\"1\",\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.craft.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}','2016-12-20 19:47:28','2016-12-20 19:47:28','a6bf7293-4221-4959-9ae5-772dfa40d706'),
	(3,3,2,1,'en_us',1,NULL,'{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"We just installed Craft!\",\"slug\":\"we-just-installed-craft\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2016-12-20 19:47:29','2016-12-20 19:47:29','f0b72076-0612-4b00-bbc8-7dd29fec4f52'),
	(4,2,1,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.craft.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"16\":\"\",\"17\":\"test\"}}','2016-12-20 20:11:45','2016-12-20 20:11:45','6b996ebb-e326-4eeb-81fd-21a9814512f4'),
	(5,2,1,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"new1\":{\"type\":\"contentBuilderContent\",\"enabled\":\"1\",\"fields\":{\"richText\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"new2\":{\"type\":\"twoColumn\",\"enabled\":\"1\",\"fields\":{\"contentLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"contentRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}}},\"16\":\"\"}}','2016-12-20 21:17:19','2016-12-20 21:17:19','d39cfa9d-ce89-4bae-8a32-6e8f4f8fc926'),
	(6,2,1,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"6\":{\"type\":\"contentBuilderContent\",\"enabled\":\"1\",\"fields\":{\"fullContent\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"7\":{\"type\":\"twoColumn\",\"enabled\":\"1\",\"fields\":{\"contentLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"contentRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}}},\"16\":\"\"}}','2016-12-20 21:18:45','2016-12-20 21:18:45','01079caa-cdf4-43eb-9855-95bbf0e9cb3e'),
	(7,2,1,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumn\",\"enabled\":\"1\",\"fields\":{\"contentLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"contentRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"contentBuilderContent\",\"enabled\":\"1\",\"fields\":{\"fullContent\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}}},\"16\":\"\"}}','2016-12-20 21:20:20','2016-12-20 21:20:20','82fdda5a-f79b-4562-8177-a6851dd3e499'),
	(8,2,1,1,'en_us',7,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"fullCopy\",\"enabled\":\"1\",\"fields\":{\"copy\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"new1\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}}},\"16\":\"\"}}','2016-12-20 21:52:55','2016-12-20 21:52:55','21fb379a-4f0c-4b6b-a1e7-145668a54809'),
	(9,2,1,1,'en_us',8,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"fullCopy\",\"enabled\":\"1\",\"fields\":{\"copy\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"8\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"new1\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p><strong><\\/strong>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}}},\"16\":\"\"}}','2016-12-20 21:53:39','2016-12-20 21:53:39','272f83b1-bc60-47cf-8c9b-933d0ffe8bf6'),
	(10,2,1,1,'en_us',9,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"fullCopy\",\"enabled\":\"1\",\"fields\":{\"copy\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"8\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"9\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"new1\":{\"type\":\"photoGallery\",\"enabled\":\"1\"}},\"16\":\"\"}}','2016-12-20 22:18:51','2016-12-20 22:18:51','30e3323c-c183-4902-940d-8928d1fcea28'),
	(11,2,1,1,'en_us',10,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"fullCopy\",\"enabled\":\"1\",\"fields\":{\"copy\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"8\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"9\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"10\":{\"type\":\"photoGallery\",\"enabled\":\"1\",\"fields\":{\"photos\":[\"13\",\"11\",\"12\"]}}},\"16\":\"\"}}','2016-12-20 22:21:47','2016-12-20 22:21:47','4142deb1-aca7-44f9-97cc-821f6e701d67'),
	(12,2,1,1,'en_us',11,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"fullCopy\",\"enabled\":\"1\",\"fields\":{\"copy\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"8\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"9\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"10\":{\"type\":\"photoGallery\",\"enabled\":\"1\",\"fields\":{\"photos\":[\"13\",\"11\",\"12\"]}}},\"16\":\"\"}}','2016-12-20 22:21:48','2016-12-20 22:21:48','9babc893-05bd-4451-ab2f-3392b4bc5b6b');

/*!40000 ALTER TABLE `craft_entryversions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldgroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldgroups`;

CREATE TABLE `craft_fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldgroups` WRITE;
/*!40000 ALTER TABLE `craft_fieldgroups` DISABLE KEYS */;

INSERT INTO `craft_fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','2016-12-20 19:47:28','2016-12-20 19:47:28','19cd2ad5-eea7-4ff3-b1c6-09c437c8fadf'),
	(2,'Social Media','2016-12-20 19:48:33','2016-12-20 19:48:33','e2e90bfb-889d-4b04-8246-6d8f24c41f65'),
	(3,'Site Controls','2016-12-20 19:49:31','2016-12-20 19:49:31','ffd54007-3942-496b-a7ab-b3225749e7df'),
	(4,'SEO','2016-12-20 19:59:35','2016-12-20 19:59:35','b3fea85c-ddbc-488f-b0dc-fd94441ef992'),
	(5,'Google Maps','2016-12-20 20:19:40','2016-12-20 20:19:40','1563f526-a0fd-47bc-8c41-46e5c59ef3fb'),
	(6,'Content Builder','2016-12-20 21:03:19','2016-12-20 21:03:19','b2706de3-ea92-4c25-9585-5a040f9238d6');

/*!40000 ALTER TABLE `craft_fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayoutfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayoutfields`;

CREATE TABLE `craft_fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `craft_fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `craft_fieldlayoutfields_tabId_fk` (`tabId`),
  KEY `craft_fieldlayoutfields_fieldId_fk` (`fieldId`),
  CONSTRAINT `craft_fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `craft_fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayoutfields` DISABLE KEYS */;

INSERT INTO `craft_fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,5,2,1,1,1,'2016-12-20 19:47:28','2016-12-20 19:47:28','44cf1a1e-5a93-46ab-85a3-1a7d73b399fd'),
	(3,5,2,2,0,2,'2016-12-20 19:47:28','2016-12-20 19:47:28','ad7e792a-da39-475c-8d6f-2eee6bc4289f'),
	(10,8,4,12,0,1,'2016-12-20 19:53:48','2016-12-20 19:53:48','2e17fe27-3409-442f-80a4-8a602124d560'),
	(11,8,4,13,0,2,'2016-12-20 19:53:48','2016-12-20 19:53:48','6d7cce23-f2b9-4393-8c5e-ee6aec0705c0'),
	(12,8,4,11,0,3,'2016-12-20 19:53:48','2016-12-20 19:53:48','785d6c56-cff3-46b6-a641-4f40617b468b'),
	(13,8,4,14,0,4,'2016-12-20 19:53:48','2016-12-20 19:53:48','ac65b906-49ed-40dd-931d-9f74670a2c7f'),
	(14,9,5,6,0,1,'2016-12-20 19:57:07','2016-12-20 19:57:07','6b3c4a67-8116-4505-882e-be51b82c764c'),
	(15,9,5,7,0,2,'2016-12-20 19:57:07','2016-12-20 19:57:07','98505130-b9a6-4349-b7da-f9de0d526eb7'),
	(16,9,5,10,0,3,'2016-12-20 19:57:07','2016-12-20 19:57:07','33b097dc-2201-4fc9-a94f-bae6a0c3c3bf'),
	(17,9,5,9,0,4,'2016-12-20 19:57:07','2016-12-20 19:57:07','cbb1ffc8-137a-43ff-a332-9ba5b002a4d0'),
	(18,9,5,15,0,5,'2016-12-20 19:57:07','2016-12-20 19:57:07','77dae403-629f-4873-b792-50311bd9118e'),
	(19,9,5,8,0,6,'2016-12-20 19:57:07','2016-12-20 19:57:07','3a82e7a9-9a00-44ef-8a24-3df7fb7af58a'),
	(20,9,5,5,0,7,'2016-12-20 19:57:07','2016-12-20 19:57:07','530864b5-fb4e-4df7-b897-8d819e8ae84b'),
	(29,14,12,20,0,1,'2016-12-20 21:10:17','2016-12-20 21:10:17','2914d1b7-5d2b-4f60-9d4c-4294a268ac85'),
	(30,14,13,16,0,1,'2016-12-20 21:10:17','2016-12-20 21:10:17','9db8e8b0-363d-43af-a8e1-26f1394e62f0'),
	(84,49,46,21,0,1,'2016-12-20 22:27:18','2016-12-20 22:27:18','935e2998-02d4-4c6f-99da-ae99785b2b91'),
	(85,50,47,22,0,1,'2016-12-20 22:27:18','2016-12-20 22:27:18','c010bfff-6415-4f61-9c82-fd627a3fbd18'),
	(86,50,47,23,0,2,'2016-12-20 22:27:18','2016-12-20 22:27:18','7d362553-5b9f-408d-b38e-055e6f2374a2'),
	(87,51,48,24,0,1,'2016-12-20 22:27:18','2016-12-20 22:27:18','cf807692-8442-4a6a-99fc-23988e6f0d03'),
	(88,51,48,25,0,2,'2016-12-20 22:27:18','2016-12-20 22:27:18','84117a63-51eb-4b2b-b267-37e618803024'),
	(89,51,48,26,0,3,'2016-12-20 22:27:18','2016-12-20 22:27:18','9aede222-8e82-49c9-b300-fa2caa4034aa'),
	(90,52,49,27,0,1,'2016-12-20 22:27:18','2016-12-20 22:27:18','28022673-f285-4cbe-99fd-ece72b899ecd'),
	(91,53,50,28,0,1,'2016-12-20 22:27:18','2016-12-20 22:27:18','522bfe99-22e2-439b-a7a2-91cb46a07620');

/*!40000 ALTER TABLE `craft_fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayouts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayouts`;

CREATE TABLE `craft_fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayouts` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayouts` DISABLE KEYS */;

INSERT INTO `craft_fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Tag','2016-12-20 19:47:28','2016-12-20 19:47:28','57acc433-8a96-4199-ab0a-83ec1a617f70'),
	(5,'Entry','2016-12-20 19:47:28','2016-12-20 19:47:28','f66fd482-ba4e-4ddd-af00-2032c979bd09'),
	(8,'GlobalSet','2016-12-20 19:53:48','2016-12-20 19:53:48','9191d9f4-5ad2-4a54-b5c6-350c1ee35f09'),
	(9,'GlobalSet','2016-12-20 19:57:07','2016-12-20 19:57:07','e9abf1fa-6768-4188-b98f-031a30ed2fe9'),
	(14,'Entry','2016-12-20 21:10:17','2016-12-20 21:10:17','776758ab-c433-4499-bcd9-1e80a1107f22'),
	(49,'MatrixBlock','2016-12-20 22:27:18','2016-12-20 22:27:18','ba3fa985-ff13-4d19-8018-1d95110aded5'),
	(50,'MatrixBlock','2016-12-20 22:27:18','2016-12-20 22:27:18','966fcde0-34f8-4fec-b265-5cf993096bd4'),
	(51,'MatrixBlock','2016-12-20 22:27:18','2016-12-20 22:27:18','cc4337fa-cafc-45d5-9ed0-e8baaddf78e4'),
	(52,'MatrixBlock','2016-12-20 22:27:18','2016-12-20 22:27:18','104c6bf7-6353-438a-a2b4-ce2926fa165d'),
	(53,'MatrixBlock','2016-12-20 22:27:18','2016-12-20 22:27:18','7addc204-c6ff-4f5b-a82a-979523bbd912'),
	(54,'Asset','2016-12-20 23:18:30','2016-12-20 23:18:30','c4eb1f9a-8c65-45cd-9997-8af54f88b4ad');

/*!40000 ALTER TABLE `craft_fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayouttabs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayouttabs`;

CREATE TABLE `craft_fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `craft_fieldlayouttabs_layoutId_fk` (`layoutId`),
  CONSTRAINT `craft_fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayouttabs` DISABLE KEYS */;

INSERT INTO `craft_fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,5,'Content',1,'2016-12-20 19:47:28','2016-12-20 19:47:28','19d9f21f-8196-4ba5-b326-239784c850d1'),
	(4,8,'Content',1,'2016-12-20 19:53:48','2016-12-20 19:53:48','75db605d-01d9-4a7e-894f-c597e4551f11'),
	(5,9,'Content',1,'2016-12-20 19:57:07','2016-12-20 19:57:07','a578fec4-76d0-45d0-aecb-a1d8e6b402d3'),
	(12,14,'Content Builder',1,'2016-12-20 21:10:17','2016-12-20 21:10:17','4d8fda82-d3f1-4df7-8730-118d81336947'),
	(13,14,'SEO',2,'2016-12-20 21:10:17','2016-12-20 21:10:17','b36c53e3-2cea-41c6-a80f-bde01a050654'),
	(46,49,'Content',1,'2016-12-20 22:27:18','2016-12-20 22:27:18','3e4861ae-5861-4d97-aee1-0ffb27253ad9'),
	(47,50,'Content',1,'2016-12-20 22:27:18','2016-12-20 22:27:18','b2a3f90e-0978-4774-9e4e-86f8eaf9e999'),
	(48,51,'Content',1,'2016-12-20 22:27:18','2016-12-20 22:27:18','b2ba47c8-9043-4d9c-b893-8f996ed9e078'),
	(49,52,'Content',1,'2016-12-20 22:27:18','2016-12-20 22:27:18','ad8004ec-4f30-4f78-8317-a25624f2200a'),
	(50,53,'Content',1,'2016-12-20 22:27:18','2016-12-20 22:27:18','b4e8b4e2-6979-4910-84c3-43e13c53d901');

/*!40000 ALTER TABLE `craft_fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fields`;

CREATE TABLE `craft_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(58) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `instructions` text COLLATE utf8_unicode_ci,
  `translatable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `craft_fields_context_idx` (`context`),
  KEY `craft_fields_groupId_fk` (`groupId`),
  CONSTRAINT `craft_fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fields` WRITE;
/*!40000 ALTER TABLE `craft_fields` DISABLE KEYS */;

INSERT INTO `craft_fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `translatable`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'Body','body','global',NULL,1,'RichText','{\"configFile\":\"Standard.json\",\"columnType\":\"text\"}','2016-12-20 19:47:28','2016-12-20 19:47:28','874f3767-79c4-4bd5-b1fb-b55988efdd10'),
	(2,1,'Tags','tags','global',NULL,0,'Tags','{\"source\":\"taggroup:1\"}','2016-12-20 19:47:28','2016-12-20 19:47:28','d431c8b8-9696-468e-810b-0ccd0e368bc9'),
	(5,2,'Flickr','flickr','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:48:55','2016-12-20 19:48:55','9cfff6e0-e6c5-48c7-805f-25c31e5ed2c7'),
	(6,2,'Facebook','facebook','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:05','2016-12-20 19:49:05','b2614393-1681-4418-a13a-9dd19ca04f2d'),
	(7,2,'Twitter','twitter','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:09','2016-12-20 19:49:09','8796aa85-8931-4f1a-bac8-faa27b435618'),
	(8,2,'Google','google','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:12','2016-12-20 19:49:12','5b565d7b-f17f-4c64-991c-e4e5c1cadaa9'),
	(9,2,'Pinterest','pinterest','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:21','2016-12-20 19:49:21','97488c60-b71a-4ff6-b60e-0c9c896b0c72'),
	(10,2,'Linkedin','linkedin','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:24','2016-12-20 19:49:24','543967b2-a3a4-4481-a9f5-62b6cb202d89'),
	(11,3,'Primary Telephone','primaryTelephone','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:58','2016-12-20 19:49:58','5ec37773-23d6-448f-8c12-e9b5f2ff51a7'),
	(12,3,'GA Code','gaCode','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:50:02','2016-12-20 19:50:02','321a9647-eca9-4a0e-9c5d-b1cbe9ff9ec0'),
	(13,3,'Primary Email','primaryEmail','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:50:07','2016-12-20 19:50:07','625fc7b4-dbfe-4277-bd7f-63ccafcc2e23'),
	(14,3,'Default SEO','defaultSeo','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:50:31','2016-12-20 19:50:31','b5360e48-fb41-44c2-aa89-96fcd7d695f2'),
	(15,2,'Instagram','instagram','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:56:57','2016-12-20 19:56:57','34811a59-a9c1-4720-8d02-cfa57401d05f'),
	(16,4,'Page Description','pageDescription','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:59:44','2016-12-20 19:59:44','c28e4b2f-8883-431a-9402-52d3fbc54587'),
	(18,5,'Street','street','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 20:19:57','2016-12-20 20:20:07','1bcf732c-68ce-4131-9f9d-dc67431af078'),
	(19,5,'City','city','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 20:20:01','2016-12-20 20:20:01','b4028fd2-1cae-4a84-9b4a-b44ec43cb71a'),
	(20,6,'Content Builder','contentBuilder','global','',0,'Matrix','{\"maxBlocks\":null}','2016-12-20 21:05:47','2016-12-20 22:27:18','0b737813-8bab-4bde-8617-c99f82e0594d'),
	(21,NULL,'Content','copy','matrixBlockType:1','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:05:47','2016-12-20 22:27:18','69733c1d-aea4-4967-94ca-d987ddfac8ef'),
	(22,NULL,'Content Left','copyLeft','matrixBlockType:2','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:05:47','2016-12-20 22:27:18','d4f2c039-51f7-4d9a-a2f3-723726c47033'),
	(23,NULL,'Content Right','copyRight','matrixBlockType:2','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:05:47','2016-12-20 22:27:18','077ed6f6-5727-4c2f-86e9-7a3c7f91c81d'),
	(24,NULL,'Content Left','copyLeft','matrixBlockType:3','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:51:19','2016-12-20 22:27:18','bb118001-873e-42d2-97bc-e73184c6c79f'),
	(25,NULL,'Content Middle','copyMiddle','matrixBlockType:3','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:51:19','2016-12-20 22:27:18','18832545-9e7d-4679-864a-701a5aca3399'),
	(26,NULL,'Content Right','copyRight','matrixBlockType:3','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:51:19','2016-12-20 22:27:18','3f1407a8-e309-49bf-9139-b6b4a580ea7a'),
	(27,NULL,'Code','code','matrixBlockType:4','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 21:59:09','2016-12-20 22:27:18','78be17a6-ecc9-44f6-900d-f72c7f4b8bb6'),
	(28,NULL,'Photos','photos','matrixBlockType:5','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":[\"folder:1\"],\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2016-12-20 22:16:22','2016-12-20 22:27:18','2b40bcb8-e9ca-4317-9158-8587b5f915da');

/*!40000 ALTER TABLE `craft_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_globalsets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_globalsets`;

CREATE TABLE `craft_globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `craft_globalsets_handle_unq_idx` (`handle`),
  KEY `craft_globalsets_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_globalsets` WRITE;
/*!40000 ALTER TABLE `craft_globalsets` DISABLE KEYS */;

INSERT INTO `craft_globalsets` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(4,'Social Media','socialMedia',9,'2016-12-20 19:48:12','2016-12-20 19:57:07','5e7c24d4-f38b-4978-83c2-b4b58ab73cf3'),
	(5,'Site Controls','siteControls',8,'2016-12-20 19:53:48','2016-12-20 19:53:48','eb6d7773-0171-48bb-982c-196bd9d992cf');

/*!40000 ALTER TABLE `craft_globalsets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_info`;

CREATE TABLE `craft_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `edition` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `siteName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `siteUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_info` WRITE;
/*!40000 ALTER TABLE `craft_info` DISABLE KEYS */;

INSERT INTO `craft_info` (`id`, `version`, `schemaVersion`, `edition`, `siteName`, `siteUrl`, `timezone`, `on`, `maintenance`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'2.6.2954','2.6.9',0,'Local Craft','http://local.craft.dev','UTC',1,0,'2016-12-20 19:47:24','2016-12-20 19:47:24','b8a3bc43-e7ba-42c4-9b93-41c2b0978a5e');

/*!40000 ALTER TABLE `craft_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_leads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_leads`;

CREATE TABLE `craft_leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_locales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_locales`;

CREATE TABLE `craft_locales` (
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`locale`),
  KEY `craft_locales_sortOrder_idx` (`sortOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_locales` WRITE;
/*!40000 ALTER TABLE `craft_locales` DISABLE KEYS */;

INSERT INTO `craft_locales` (`locale`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	('en_us',1,'2016-12-20 19:47:24','2016-12-20 19:47:24','3ded7672-9584-498e-9558-c566ad6dc8dc');

/*!40000 ALTER TABLE `craft_locales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixblocks`;

CREATE TABLE `craft_matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_matrixblocks_ownerId_idx` (`ownerId`),
  KEY `craft_matrixblocks_fieldId_idx` (`fieldId`),
  KEY `craft_matrixblocks_typeId_idx` (`typeId`),
  KEY `craft_matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `craft_matrixblocks_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `craft_matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_matrixblocks` WRITE;
/*!40000 ALTER TABLE `craft_matrixblocks` DISABLE KEYS */;

INSERT INTO `craft_matrixblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `sortOrder`, `ownerLocale`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(6,2,20,1,2,NULL,'2016-12-20 21:17:19','2016-12-20 22:21:48','a4eab007-203a-43d2-ae39-1a335f982469'),
	(7,2,20,2,1,NULL,'2016-12-20 21:17:19','2016-12-20 22:21:48','70fc193c-1a0e-441d-b2d5-e04fc7c691be'),
	(8,2,20,3,3,NULL,'2016-12-20 21:52:55','2016-12-20 22:21:48','19c92bab-5fae-492b-aaa9-3cb61389c5f8'),
	(9,2,20,3,4,NULL,'2016-12-20 21:53:39','2016-12-20 22:21:48','b8075e45-08a3-4ced-a1bd-6cf26f8f7930'),
	(10,2,20,5,5,NULL,'2016-12-20 22:18:51','2016-12-20 22:21:48','d12a0cf0-dc0c-4827-b0e8-60debca991bc');

/*!40000 ALTER TABLE `craft_matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixblocktypes`;

CREATE TABLE `craft_matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `craft_matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `craft_matrixblocktypes_fieldId_fk` (`fieldId`),
  KEY `craft_matrixblocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `craft_matrixblocktypes` DISABLE KEYS */;

INSERT INTO `craft_matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,20,49,'Full Width','fullCopy',1,'2016-12-20 21:05:47','2016-12-20 22:27:18','7f567c6b-a0b3-4531-9369-ba539971fb50'),
	(2,20,50,'Two Column','twoColumnCopy',2,'2016-12-20 21:05:47','2016-12-20 22:27:18','ac12f441-ff2b-4ada-b8df-9f4e92ac2402'),
	(3,20,51,'Three Column','threeColumnCopy',3,'2016-12-20 21:51:19','2016-12-20 22:27:18','e4dd6d80-b5f0-4092-9dc9-02474f96a2d6'),
	(4,20,52,'Code Snippet','codeSnippet',4,'2016-12-20 21:59:09','2016-12-20 22:27:18','48cc282d-b604-4ac1-b84e-5b5757dcfc2a'),
	(5,20,53,'Photo Gallery','photoGallery',5,'2016-12-20 22:16:22','2016-12-20 22:27:18','f70425fc-ee45-47f2-be6a-65fb21555c4b');

/*!40000 ALTER TABLE `craft_matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixcontent_contentbuilder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixcontent_contentbuilder`;

CREATE TABLE `craft_matrixcontent_contentbuilder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_fullCopy_copy` text COLLATE utf8_unicode_ci,
  `field_twoColumnCopy_copyLeft` text COLLATE utf8_unicode_ci,
  `field_twoColumnCopy_copyRight` text COLLATE utf8_unicode_ci,
  `field_threeColumnCopy_copyLeft` text COLLATE utf8_unicode_ci,
  `field_threeColumnCopy_copyMiddle` text COLLATE utf8_unicode_ci,
  `field_threeColumnCopy_copyRight` text COLLATE utf8_unicode_ci,
  `field_codeSnippet_code` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_matrixcontent_contentbuilder_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `craft_matrixcontent_contentbuilder_locale_fk` (`locale`),
  CONSTRAINT `craft_matrixcontent_contentbuilder_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixcontent_contentbuilder_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_matrixcontent_contentbuilder` WRITE;
/*!40000 ALTER TABLE `craft_matrixcontent_contentbuilder` DISABLE KEYS */;

INSERT INTO `craft_matrixcontent_contentbuilder` (`id`, `elementId`, `locale`, `field_fullCopy_copy`, `field_twoColumnCopy_copyLeft`, `field_twoColumnCopy_copyRight`, `field_threeColumnCopy_copyLeft`, `field_threeColumnCopy_copyMiddle`, `field_threeColumnCopy_copyRight`, `field_codeSnippet_code`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,6,'en_us','<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>',NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 21:17:19','2016-12-20 22:21:48','d92f9bc7-b564-488d-afce-a8c7ffd8274b'),
	(2,7,'en_us',NULL,'<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>','<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>',NULL,NULL,NULL,NULL,'2016-12-20 21:17:19','2016-12-20 22:21:48','eec51011-d4f5-4f5d-944b-aaa3b4e73978'),
	(3,8,'en_us',NULL,NULL,NULL,'<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>','<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>','<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>',NULL,'2016-12-20 21:52:55','2016-12-20 22:21:48','0167a326-a8cb-4eeb-b5fb-44ac1157ab61'),
	(4,9,'en_us',NULL,NULL,NULL,'<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>','<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>','<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>',NULL,'2016-12-20 21:53:39','2016-12-20 22:21:48','29854e39-2a3c-4a35-97de-f25da6222938'),
	(5,10,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 22:18:51','2016-12-20 22:21:48','21bea68b-36ed-40b1-97fa-f3abfe645dec');

/*!40000 ALTER TABLE `craft_matrixcontent_contentbuilder` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_migrations`;

CREATE TABLE `craft_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_migrations_version_unq_idx` (`version`),
  KEY `craft_migrations_pluginId_fk` (`pluginId`),
  CONSTRAINT `craft_migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `craft_plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_migrations` WRITE;
/*!40000 ALTER TABLE `craft_migrations` DISABLE KEYS */;

INSERT INTO `craft_migrations` (`id`, `pluginId`, `version`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'m000000_000000_base','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','81136106-f88f-490d-bbbd-2abfe6025bf6'),
	(2,NULL,'m140730_000001_add_filename_and_format_to_transformindex','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','998b0367-9458-486c-a2ef-9e3c46fb2b94'),
	(3,NULL,'m140815_000001_add_format_to_transforms','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','5ecdc0ac-157a-43f9-a24d-cbbfb476049b'),
	(4,NULL,'m140822_000001_allow_more_than_128_items_per_field','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','5ec71ebd-2e18-45b7-bb7b-f9d64a55f121'),
	(5,NULL,'m140829_000001_single_title_formats','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','75e72af3-737b-478a-bb07-730c94791662'),
	(6,NULL,'m140831_000001_extended_cache_keys','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','b75eb466-52f2-4c5d-80a1-60025775f233'),
	(7,NULL,'m140922_000001_delete_orphaned_matrix_blocks','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','e7638bcc-7cf5-4d51-abff-55121ca23a80'),
	(8,NULL,'m141008_000001_elements_index_tune','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','3e2ca63e-ab5d-443b-a3d3-5343abb522a2'),
	(9,NULL,'m141009_000001_assets_source_handle','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','50343f76-d626-466a-8b0e-90565edbe021'),
	(10,NULL,'m141024_000001_field_layout_tabs','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','50b2b7a3-f005-45b9-86cf-5799c1fcb697'),
	(11,NULL,'m141030_000000_plugin_schema_versions','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','120755f5-a29d-45d1-822f-38837f4b6ee9'),
	(12,NULL,'m141030_000001_drop_structure_move_permission','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','b5804895-4a40-44dc-8a58-04bc3adcb8aa'),
	(13,NULL,'m141103_000001_tag_titles','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','447a56e3-d4bc-4f66-a359-70e86d90651b'),
	(14,NULL,'m141109_000001_user_status_shuffle','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','47ada619-aff7-461f-8313-062d54086094'),
	(15,NULL,'m141126_000001_user_week_start_day','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','b5cec8db-f2cc-4fe0-93bb-30917165dd7a'),
	(16,NULL,'m150210_000001_adjust_user_photo_size','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','f0837d36-4763-4dc2-b941-4abe1de103e1'),
	(17,NULL,'m150724_000001_adjust_quality_settings','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','72a9bc4a-c2d7-42c3-93d9-53dd4d1e2abc'),
	(18,NULL,'m150827_000000_element_index_settings','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','2476b917-9576-4b31-8292-d8ebc516a573'),
	(19,NULL,'m150918_000001_add_colspan_to_widgets','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','c26d4634-1055-494e-82ac-57a2515e358e'),
	(20,NULL,'m151007_000000_clear_asset_caches','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','41b8b36a-4131-4fc1-96ca-ea33e6ca81ed'),
	(21,NULL,'m151109_000000_text_url_formats','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','fb866ffd-5ac6-4737-8212-edc0102286ab'),
	(22,NULL,'m151110_000000_move_logo','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','1d9ec314-bf46-4646-88f2-1ef0da48beca'),
	(23,NULL,'m151117_000000_adjust_image_widthheight','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','4e5b2be3-a938-4863-b733-220b686b778e'),
	(24,NULL,'m151127_000000_clear_license_key_status','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','ba6b62d5-8ca8-4a0e-a2b9-428d9c54dc4a'),
	(25,NULL,'m151127_000000_plugin_license_keys','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','69c66fce-d189-459b-8ba2-766f6c7da672'),
	(26,NULL,'m151130_000000_update_pt_widget_feeds','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','203ca132-f8f9-4de5-8414-8da166e261ec'),
	(27,NULL,'m160114_000000_asset_sources_public_url_default_true','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','678b9680-d3f7-446f-94f6-35f17c699456'),
	(28,NULL,'m160223_000000_sortorder_to_smallint','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','aa4fbeb7-dfe9-4c20-8d6d-13a4fbf4b4ae'),
	(29,NULL,'m160229_000000_set_default_entry_statuses','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','826453de-efaf-45df-9d59-68cd8516fe9e'),
	(30,NULL,'m160304_000000_client_permissions','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','db385a8a-e2c6-4f87-84d1-981496e5905c'),
	(31,NULL,'m160322_000000_asset_filesize','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','5c8940ac-e13e-4639-a545-6b692e6f75d6'),
	(32,NULL,'m160503_000000_orphaned_fieldlayouts','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','fe4bc8be-ce78-4a54-bc8b-d62eb391810d'),
	(33,NULL,'m160510_000000_tasksettings','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','e0e008ed-bb56-46c1-90c1-8224f051a47f'),
	(34,NULL,'m160829_000000_pending_user_content_cleanup','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','b881082d-c3ed-4cc0-a239-5b5afaf393e7'),
	(35,NULL,'m160830_000000_asset_index_uri_increase','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','cc84c475-bd75-4e76-afb8-8bdc4d85907c'),
	(36,NULL,'m160919_000000_usergroup_handle_title_unique','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','d5197075-cc4d-47ce-90d5-634e5261af0d'),
	(37,NULL,'m161108_000000_new_version_format','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','5bc97a3b-78fc-4466-aa21-e3b892571d2c'),
	(38,NULL,'m161109_000000_index_shuffle','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','7f3e93e9-9e2d-466d-bda3-2ec1c898366a'),
	(39,3,'m151016_151424_pimpmymatrix_add_block_types_table','2016-12-20 21:39:17','2016-12-20 21:39:17','2016-12-20 21:39:17','21ea5fbc-8b26-4010-91f8-0775460f62c9'),
	(40,3,'m151027_103010_pimpmymatrix_migrate_old_settings','2016-12-20 21:39:17','2016-12-20 21:39:17','2016-12-20 21:39:17','2b871a77-7870-4286-b821-9b0fcf89a825');

/*!40000 ALTER TABLE `craft_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_pimpmymatrix_blocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_pimpmymatrix_blocktypes`;

CREATE TABLE `craft_pimpmymatrix_blocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `matrixBlockTypeId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `groupName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_pimpmymatrix_blocktypes_fieldId_fk` (`fieldId`),
  KEY `craft_pimpmymatrix_blocktypes_matrixBlockTypeId_fk` (`matrixBlockTypeId`),
  KEY `craft_pimpmymatrix_blocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_pimpmymatrix_blocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_pimpmymatrix_blocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_pimpmymatrix_blocktypes_matrixBlockTypeId_fk` FOREIGN KEY (`matrixBlockTypeId`) REFERENCES `craft_matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_pimpmymatrix_blocktypes` WRITE;
/*!40000 ALTER TABLE `craft_pimpmymatrix_blocktypes` DISABLE KEYS */;

INSERT INTO `craft_pimpmymatrix_blocktypes` (`id`, `fieldId`, `matrixBlockTypeId`, `fieldLayoutId`, `groupName`, `context`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(10,20,1,NULL,'Content','global','2016-12-20 22:18:18','2016-12-20 22:18:18','8be5421c-8470-4bfd-b3ac-472a52b8bb79'),
	(11,20,2,NULL,'Content','global','2016-12-20 22:18:18','2016-12-20 22:18:18','b707dfab-7001-4e91-90e0-3375ba0e1e1b'),
	(12,20,3,NULL,'Content','global','2016-12-20 22:18:18','2016-12-20 22:18:18','0e91475c-9474-489e-8c67-933588898301'),
	(13,20,4,NULL,'Content','global','2016-12-20 22:18:18','2016-12-20 22:18:18','62a24857-6faf-42ea-8941-c8bd4adb1836'),
	(14,20,5,NULL,'Images','global','2016-12-20 22:18:18','2016-12-20 22:18:18','f0a4e728-9cb1-407c-99b6-ca8b736f74ad');

/*!40000 ALTER TABLE `craft_pimpmymatrix_blocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_plugins`;

CREATE TABLE `craft_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKey` char(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','unknown') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `settings` text COLLATE utf8_unicode_ci,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_plugins` WRITE;
/*!40000 ALTER TABLE `craft_plugins` DISABLE KEYS */;

INSERT INTO `craft_plugins` (`id`, `class`, `version`, `schemaVersion`, `licenseKey`, `licenseKeyStatus`, `enabled`, `settings`, `installDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Leads','1.0',NULL,NULL,'unknown',1,'{\"toEmail\":\"justin.lobaito@weloideas.com\",\"adminSubject\":\"New message from Local Craft\",\"guestSubject\":\"Hello from Local Craft\",\"welcomeEmailMessage\":\"<p>Hi {{name}} &mdash; thank you for reaching out to us!<br>We wanted to drop you a quick note that we\'ve received your information and we\'ll be in touch very soon!<\\/p><p>Regards,<br>Local Craft<\\/p>\"}','2016-12-20 20:49:09','2016-12-20 20:49:09','2016-12-20 20:59:29','8a300c1e-7c46-40e1-bae5-b5f20ce507ac'),
	(2,'Blueprint','0.5',NULL,NULL,'unknown',1,NULL,'2016-12-20 21:35:25','2016-12-20 21:35:25','2016-12-20 21:35:25','b0453b63-6478-46e5-a4d7-c8725c257738'),
	(3,'PimpMyMatrix','2.1.2','2.0.0',NULL,'unknown',1,NULL,'2016-12-20 21:39:17','2016-12-20 21:39:17','2016-12-20 21:39:17','117eea4c-eb69-4abf-8e79-8e41a6d2506e');

/*!40000 ALTER TABLE `craft_plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_rackspaceaccess
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_rackspaceaccess`;

CREATE TABLE `craft_rackspaceaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connectionKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storageUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdnUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_rackspaceaccess_connectionKey_unq_idx` (`connectionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_relations`;

CREATE TABLE `craft_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_relations_fieldId_sourceId_sourceLocale_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceLocale`,`targetId`),
  KEY `craft_relations_sourceId_fk` (`sourceId`),
  KEY `craft_relations_sourceLocale_fk` (`sourceLocale`),
  KEY `craft_relations_targetId_fk` (`targetId`),
  CONSTRAINT `craft_relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_relations_sourceLocale_fk` FOREIGN KEY (`sourceLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_relations` WRITE;
/*!40000 ALTER TABLE `craft_relations` DISABLE KEYS */;

INSERT INTO `craft_relations` (`id`, `fieldId`, `sourceId`, `sourceLocale`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(4,28,10,NULL,13,1,'2016-12-20 22:21:48','2016-12-20 22:21:48','923df263-5bf0-4e1d-ae0c-c426b8f02a43'),
	(5,28,10,NULL,11,2,'2016-12-20 22:21:48','2016-12-20 22:21:48','dbe8e064-615a-470d-b9b6-366bb1fddffa'),
	(6,28,10,NULL,12,3,'2016-12-20 22:21:48','2016-12-20 22:21:48','07d626f1-e203-41c9-bcbd-77ea3da29039');

/*!40000 ALTER TABLE `craft_relations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_routes`;

CREATE TABLE `craft_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlParts` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urlPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_routes_urlPattern_unq_idx` (`urlPattern`),
  KEY `craft_routes_locale_idx` (`locale`),
  CONSTRAINT `craft_routes_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_searchindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_searchindex`;

CREATE TABLE `craft_searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`locale`),
  FULLTEXT KEY `craft_searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_searchindex` WRITE;
/*!40000 ALTER TABLE `craft_searchindex` DISABLE KEYS */;

INSERT INTO `craft_searchindex` (`elementId`, `attribute`, `fieldId`, `locale`, `keywords`)
VALUES
	(1,'username',0,'en_us',' admin '),
	(1,'firstname',0,'en_us',''),
	(1,'lastname',0,'en_us',''),
	(1,'fullname',0,'en_us',''),
	(1,'email',0,'en_us',' justin lobaito weloideas com '),
	(1,'slug',0,'en_us',''),
	(2,'slug',0,'en_us',' homepage '),
	(2,'title',0,'en_us',' welcome to local craft dev '),
	(2,'field',1,'en_us',' it s true this site doesn t have a whole lot of content yet but don t worry our web developers have just installed the cms and they re setting things up for the content editors this very moment soon local craft dev will be an oasis of fresh perspectives sharp analyses and astute opinions that will keep you coming back again and again '),
	(3,'field',1,'en_us',' craft is the cms that s powering local craft dev it s beautiful powerful flexible and easy to use and it s made by pixel tonic we can t wait to dive in and see what it s capable of this is even more captivating content which you couldn t see on the news index page because it was entered after a page break and the news index template only likes to show the content on the first page craft a nice alternative to word if you re making a website '),
	(3,'field',2,'en_us',''),
	(3,'slug',0,'en_us',' we just installed craft '),
	(3,'title',0,'en_us',' we just installed craft '),
	(4,'slug',0,'en_us',''),
	(4,'field',6,'en_us',' test '),
	(4,'field',7,'en_us',''),
	(4,'field',10,'en_us',''),
	(4,'field',9,'en_us',''),
	(4,'field',8,'en_us',''),
	(4,'field',5,'en_us',''),
	(5,'field',12,'en_us',' test '),
	(5,'field',13,'en_us',''),
	(5,'field',11,'en_us',''),
	(5,'field',14,'en_us',''),
	(5,'slug',0,'en_us',''),
	(4,'field',15,'en_us',''),
	(2,'field',17,'en_us',' test '),
	(2,'field',16,'en_us',''),
	(2,'field',20,'en_us',' ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque screen shot 2016 12 19 at 4 40 10 pm screen shot 2016 12 20 at 12 31 37 pm screen shot 2016 12 20 at 9 42 04 am '),
	(6,'field',21,'en_us',' ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(6,'slug',0,'en_us',''),
	(7,'field',22,'en_us',' ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(7,'field',23,'en_us',' ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(7,'slug',0,'en_us',''),
	(8,'field',24,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(8,'field',25,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(8,'field',26,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(8,'slug',0,'en_us',''),
	(9,'field',24,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(9,'field',25,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(9,'field',26,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(9,'slug',0,'en_us',''),
	(10,'field',28,'en_us',' screen shot 2016 12 19 at 4 40 10 pm screen shot 2016 12 20 at 12 31 37 pm screen shot 2016 12 20 at 9 42 04 am '),
	(10,'slug',0,'en_us',''),
	(11,'filename',0,'en_us',' screen shot 2016 12 20 at 12 31 37 pm png '),
	(11,'extension',0,'en_us',' png '),
	(11,'kind',0,'en_us',' image '),
	(11,'slug',0,'en_us',' screen shot 2016 12 20 at 12 31 37 pm '),
	(11,'title',0,'en_us',' screen shot 2016 12 20 at 12 31 37 pm '),
	(12,'filename',0,'en_us',' screen shot 2016 12 20 at 9 42 04 am png '),
	(12,'extension',0,'en_us',' png '),
	(12,'kind',0,'en_us',' image '),
	(12,'slug',0,'en_us',' screen shot 2016 12 20 at 9 42 04 am '),
	(12,'title',0,'en_us',' screen shot 2016 12 20 at 9 42 04 am '),
	(13,'filename',0,'en_us',' screen shot 2016 12 19 at 4 40 10 pm png '),
	(13,'extension',0,'en_us',' png '),
	(13,'kind',0,'en_us',' image '),
	(13,'slug',0,'en_us',' screen shot 2016 12 19 at 4 40 10 pm '),
	(13,'title',0,'en_us',' screen shot 2016 12 19 at 4 40 10 pm ');

/*!40000 ALTER TABLE `craft_searchindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sections`;

CREATE TABLE `craft_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enableVersioning` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_sections_name_unq_idx` (`name`),
  UNIQUE KEY `craft_sections_handle_unq_idx` (`handle`),
  KEY `craft_sections_structureId_fk` (`structureId`),
  CONSTRAINT `craft_sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sections` WRITE;
/*!40000 ALTER TABLE `craft_sections` DISABLE KEYS */;

INSERT INTO `craft_sections` (`id`, `structureId`, `name`, `handle`, `type`, `hasUrls`, `template`, `enableVersioning`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'Homepage','homepage','single',1,'index',1,'2016-12-20 19:47:28','2016-12-20 19:47:28','83f38eb9-1ebe-4f75-9cb6-70475f746437'),
	(2,NULL,'News','news','channel',1,'news/_entry',1,'2016-12-20 19:47:28','2016-12-20 19:47:28','a71b87bb-6b2a-445f-b914-2bb1f3ca810d');

/*!40000 ALTER TABLE `craft_sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sections_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sections_i18n`;

CREATE TABLE `craft_sections_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `enabledByDefault` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_sections_i18n_sectionId_locale_unq_idx` (`sectionId`,`locale`),
  KEY `craft_sections_i18n_locale_fk` (`locale`),
  CONSTRAINT `craft_sections_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_sections_i18n_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sections_i18n` WRITE;
/*!40000 ALTER TABLE `craft_sections_i18n` DISABLE KEYS */;

INSERT INTO `craft_sections_i18n` (`id`, `sectionId`, `locale`, `enabledByDefault`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us',1,'__home__',NULL,'2016-12-20 19:47:28','2016-12-20 19:47:28','cde28200-39d6-4620-9030-5d007d1db12c'),
	(2,2,'en_us',1,'news/{postDate.year}/{slug}',NULL,'2016-12-20 19:47:28','2016-12-20 19:47:28','0b3fcca6-3e56-402c-a318-dc43b108e229');

/*!40000 ALTER TABLE `craft_sections_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sessions`;

CREATE TABLE `craft_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_sessions_uid_idx` (`uid`),
  KEY `craft_sessions_token_idx` (`token`),
  KEY `craft_sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `craft_sessions_userId_fk` (`userId`),
  CONSTRAINT `craft_sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sessions` WRITE;
/*!40000 ALTER TABLE `craft_sessions` DISABLE KEYS */;

INSERT INTO `craft_sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'a77119edd880a7aa3e42e92653b3198ed5468bddczozMjoiMFlVVFpUODVBUnRrUE9VMmh3MlYxZW1hRkFJSmFOYk0iOw==','2016-12-20 19:47:28','2016-12-20 19:47:28','d00ae958-6cc8-4200-a445-b7604dfe07ce');

/*!40000 ALTER TABLE `craft_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_shunnedmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_shunnedmessages`;

CREATE TABLE `craft_shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `craft_shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_structureelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_structureelements`;

CREATE TABLE `craft_structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `craft_structureelements_root_idx` (`root`),
  KEY `craft_structureelements_lft_idx` (`lft`),
  KEY `craft_structureelements_rgt_idx` (`rgt`),
  KEY `craft_structureelements_level_idx` (`level`),
  KEY `craft_structureelements_elementId_fk` (`elementId`),
  CONSTRAINT `craft_structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_structures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_structures`;

CREATE TABLE `craft_structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_systemsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_systemsettings`;

CREATE TABLE `craft_systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_systemsettings` WRITE;
/*!40000 ALTER TABLE `craft_systemsettings` DISABLE KEYS */;

INSERT INTO `craft_systemsettings` (`id`, `category`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'email','{\"protocol\":\"smtp\",\"emailAddress\":\"justin.lobaito@weloideas.com\",\"senderName\":\"Local Craft\",\"smtpAuth\":1,\"username\":\"jplobaito\",\"password\":\"Jpl@57006\",\"smtpSecureTransportType\":\"none\",\"port\":\"587\",\"host\":\"smtp.sendgrid.net\",\"timeout\":\"30\"}','2016-12-20 19:47:28','2016-12-20 20:26:19','97c3fee5-6e74-48b9-84fe-6428aa548cd7');

/*!40000 ALTER TABLE `craft_systemsettings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_taggroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_taggroups`;

CREATE TABLE `craft_taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_taggroups_handle_unq_idx` (`handle`),
  KEY `craft_taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_taggroups` WRITE;
/*!40000 ALTER TABLE `craft_taggroups` DISABLE KEYS */;

INSERT INTO `craft_taggroups` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','default',1,'2016-12-20 19:47:28','2016-12-20 19:47:28','452d6bfe-6158-40a9-8ed1-f1b0bdd13d41');

/*!40000 ALTER TABLE `craft_taggroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tags`;

CREATE TABLE `craft_tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_tags_groupId_fk` (`groupId`),
  CONSTRAINT `craft_tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_tags_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_tasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tasks`;

CREATE TABLE `craft_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `currentStep` int(11) unsigned DEFAULT NULL,
  `totalSteps` int(11) unsigned DEFAULT NULL,
  `status` enum('pending','error','running') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` mediumtext COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_tasks_root_idx` (`root`),
  KEY `craft_tasks_lft_idx` (`lft`),
  KEY `craft_tasks_rgt_idx` (`rgt`),
  KEY `craft_tasks_level_idx` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_templatecachecriteria
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecachecriteria`;

CREATE TABLE `craft_templatecachecriteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_templatecachecriteria_cacheId_fk` (`cacheId`),
  KEY `craft_templatecachecriteria_type_idx` (`type`),
  CONSTRAINT `craft_templatecachecriteria_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_templatecacheelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecacheelements`;

CREATE TABLE `craft_templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `craft_templatecacheelements_cacheId_fk` (`cacheId`),
  KEY `craft_templatecacheelements_elementId_fk` (`elementId`),
  CONSTRAINT `craft_templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_templatecaches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecaches`;

CREATE TABLE `craft_templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_templatecaches_expiryDate_cacheKey_locale_path_idx` (`expiryDate`,`cacheKey`,`locale`,`path`),
  KEY `craft_templatecaches_locale_fk` (`locale`),
  CONSTRAINT `craft_templatecaches_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tokens`;

CREATE TABLE `craft_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_tokens_token_unq_idx` (`token`),
  KEY `craft_tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_usergroups`;

CREATE TABLE `craft_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_usergroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_usergroups_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_usergroups_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_usergroups_users`;

CREATE TABLE `craft_usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `craft_usergroups_users_userId_fk` (`userId`),
  CONSTRAINT `craft_usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_userpermissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions`;

CREATE TABLE `craft_userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_userpermissions_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions_usergroups`;

CREATE TABLE `craft_userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `craft_userpermissions_usergroups_groupId_fk` (`groupId`),
  CONSTRAINT `craft_userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_userpermissions_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions_users`;

CREATE TABLE `craft_userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `craft_userpermissions_users_userId_fk` (`userId`),
  CONSTRAINT `craft_userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_users`;

CREATE TABLE `craft_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preferredLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weekStartDay` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `client` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `suspended` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pending` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIPAddress` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(4) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `verificationCode` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_users_username_unq_idx` (`username`),
  UNIQUE KEY `craft_users_email_unq_idx` (`email`),
  KEY `craft_users_verificationCode_idx` (`verificationCode`),
  KEY `craft_users_uid_idx` (`uid`),
  KEY `craft_users_preferredLocale_fk` (`preferredLocale`),
  CONSTRAINT `craft_users_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_users_preferredLocale_fk` FOREIGN KEY (`preferredLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_users` WRITE;
/*!40000 ALTER TABLE `craft_users` DISABLE KEYS */;

INSERT INTO `craft_users` (`id`, `username`, `photo`, `firstName`, `lastName`, `email`, `password`, `preferredLocale`, `weekStartDay`, `admin`, `client`, `locked`, `suspended`, `pending`, `archived`, `lastLoginDate`, `lastLoginAttemptIPAddress`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'admin',NULL,NULL,NULL,'justin.lobaito@weloideas.com','$2y$13$A//zMb6mPWbLo0zyZZdFO.bLd4vzFCNn7hz9maXE93hKGygt78Ffm',NULL,0,1,0,0,0,0,0,'2016-12-20 19:47:28','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2016-12-20 19:47:26','2016-12-20 19:47:26','2016-12-20 19:47:28','68e51f4c-fb0d-4062-b0df-afb60d8386a5');

/*!40000 ALTER TABLE `craft_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_widgets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_widgets`;

CREATE TABLE `craft_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(4) unsigned DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_widgets_userId_fk` (`userId`),
  CONSTRAINT `craft_widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_widgets` WRITE;
/*!40000 ALTER TABLE `craft_widgets` DISABLE KEYS */;

INSERT INTO `craft_widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'RecentEntries',1,NULL,NULL,1,'2016-12-20 19:47:30','2016-12-20 19:47:30','04511b62-40c3-4541-b5e8-caf7f7fe21bd'),
	(2,1,'GetHelp',2,NULL,NULL,1,'2016-12-20 19:47:30','2016-12-20 19:47:30','03564b4c-52c0-4258-be0f-4733a21bd2ef'),
	(3,1,'Updates',3,NULL,NULL,1,'2016-12-20 19:47:30','2016-12-20 19:47:30','7a426a67-cd08-4b9a-8a4c-64d42d438709'),
	(4,1,'Feed',4,NULL,'{\"url\":\"https:\\/\\/craftcms.com\\/news.rss\",\"title\":\"Craft News\"}',1,'2016-12-20 19:47:30','2016-12-20 19:47:30','623a6510-c0cd-4e0b-a62a-7e76500178c7');

/*!40000 ALTER TABLE `craft_widgets` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
