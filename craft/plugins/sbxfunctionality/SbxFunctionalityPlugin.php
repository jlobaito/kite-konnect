<?php

namespace Craft;

class SbxFunctionalityPlugin extends BasePlugin
{
    public function getName()
    {
        return Craft::t('Sandbox Functionality');
    }

    public function getVersion()
    {
        return '1.0';
    }

    public function getDeveloper()
    {
        return 'Kyle Brumm';
    }

    public function getDeveloperUrl()
    {
        return 'http://kylebrumm.com';
    }

    public function hasCpSection()
    {
        return false;
    }

    /**
     * Add our custom CSS and JS
     */
    public function init()
    {
        if ( craft()->request->isCpRequest() && craft()->userSession->isLoggedIn() )
        {
            // Add the CSS
            craft()->templates->includeCssResource('sbxfunctionality/css/style.css');

            // Add the JS
            craft()->templates->includeJsResource('sbxfunctionality/js/script.js');
        }
    }

    /**
     * Register twig extension
     */
    public function addTwigExtension()
    {
        Craft::import('plugins.sbxfunctionality.twigextensions.SbxFunctionalityTwigExtension');

        return new SbxFunctionalityTwigExtension();
    }
}
