<?php

namespace Craft;

/**
 * Variables provide access to database objects from templates
 */
class SbxFunctionalityVariable
{
    /**
     *  Allow the use of form macros on the front-end
     *
     *  @param   string  $macro  Macro to use
     *  @param   array   $args   Arguments for the macro
     *  @return  string          HTML to be displayed
     */
    public function renderForm($macro, array $args)
    {
        // Get the current template path
        $originalPath = craft()->path->getTemplatesPath();

        // Point Twig at the CP templates
        craft()->path->setTemplatesPath(craft()->path->getCpTemplatesPath());

        // Render the macro.
        $html = craft()->templates->renderMacro('_includes/forms', $macro, array($args));

        // Restore the original template path
        craft()->path->setTemplatesPath($originalPath);

        // Return the HTML
        return TemplateHelper::getRaw($html);
    }
}
