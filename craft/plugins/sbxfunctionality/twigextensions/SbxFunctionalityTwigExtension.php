<?php

namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class SbxFunctionalityTwigExtension extends Twig_Extension
{
    public function getName()
    {
        return 'sbxfunctionality';
    }

    public function getFilters()
    {
        return array(
            'serialize'   => new Twig_Filter_Method($this, 'serialize'),
            'unserialize' => new Twig_Filter_Method($this, 'unserialize'),
        );
    }

    /**
     * Serialize a string
     *
     * Usage: {{ value|serialize }}
     */
    public function serialize($value)
    {
        return serialize( $value );
    }

    /**
     * Unserialize a string
     *
     * Usage: {{ serializedString|unserialize }}
     */
    public function unserialize($value)
    {
        return unserialize( $value );
    }
}
